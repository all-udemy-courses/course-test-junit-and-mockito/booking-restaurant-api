package bookingrestaurantapi.bookingrestaurantapi.controller;

import java.util.Objects;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class IndexController {

    @PostMapping("/welcome")
    @ResponseBody
    public String welcome(@RequestParam(required = false, name = "params") final String[] params) {
        if (Objects.isNull(params)) {
            return "No params for process";
        } else {
            final StringBuilder msg = new StringBuilder();

            for (int index = 0; index < params.length; index++) {
                msg.append("param[" + index + "] -> " + params[index] + " ");
            }

            return msg.toString().trim();
        }
    }

}
