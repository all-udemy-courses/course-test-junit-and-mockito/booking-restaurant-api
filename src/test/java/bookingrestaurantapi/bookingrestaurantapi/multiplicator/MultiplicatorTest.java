package bookingrestaurantapi.bookingrestaurantapi.multiplicator;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MultiplicatorTest {

    private final Multiplicator multiplicator = new Multiplicator();

    @Test
    void multiply_test() {
        final int result = multiplicator.multiply(2, 2);
        final int resultExpected = 4;

        assertEquals(result, resultExpected);
    }
}
