package bookingrestaurantapi.bookingrestaurantapi.multiplicator;

public class Multiplicator {

    public int multiply(final int x, final int y) {
        return x * y;
    }
}
