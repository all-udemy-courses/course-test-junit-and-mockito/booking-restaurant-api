package bookingrestaurantapi.bookingrestaurantapi.controller;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class IndexControllerTest {

    @Test
    void nullArrayTest(){
        IndexController indexController = new IndexController();

        String[] paramsArray = null;

        String resultExpected = "No params for process";
        String result = indexController.welcome(paramsArray);

        assertEquals(resultExpected, result);
    }

    @Test
    void emptyArrayTest(){
        IndexController indexController = new IndexController();

        String[] paramsArray = new String[3];

        String resultExpected = "param[0] -> null param[1] -> null param[2] -> null";
        String result = indexController.welcome(paramsArray);

        System.out.println(resultExpected);
        System.out.println(result);

        assertEquals(resultExpected, result);
    }

    @Test
    void arrayTest(){
        IndexController indexController = new IndexController();

        String[] paramsArray = {"Jesus", "Ana", "Alanis"};

        String resultExpected = "param[0] -> Jesus param[1] -> Ana param[2] -> Alanis";
        String result = indexController.welcome(paramsArray);

        System.out.println(resultExpected);
        System.out.println(result);

        assertEquals(resultExpected, result);
    }
}
